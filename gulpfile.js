var gulp = require('gulp');
var csso = require('gulp-csso');
var cleanCSS = require('gulp-clean-css');
var minify = require('gulp-minify');
var htmlmin = require('gulp-htmlmin');
var replace = require('gulp-replace-task');
var rename = require('gulp-rename');


gulp.task('minify-main-css', function () {
     gulp.src('css/main.css')
        .pipe(csso())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('css/min'));
});


gulp.task('minify-dd-css', function () {
     gulp.src('css/dd.css')
        .pipe(csso())
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('css/min'));
});


gulp.task('minify-main-js', function () {
    gulp.src('js/main.js')
        .pipe(minify({
            ext: {
                min: '.js'
            },
        }))
        .pipe(gulp.dest('js/min'))
});

gulp.task('minify-html', function () {
    gulp.src('index.en.src.html')
        .pipe(replace({
            patterns: [
                {
                    match: /version_number/g,
                    replacement: new Date().getTime()
                }
            ]
        }))
        .pipe(replace({
            patterns: [
                {
                    match: /css\/main\.css/g,
                    replacement: 'css/min/main.css'
                }
            ]
        }))
        .pipe(replace({
            patterns: [
                {
                    match: /js\/main\.js/g,
                    replacement: 'js/min/main.js'
                }
            ]
        }))
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(rename('index.html'))
        .pipe(gulp.dest('.'));

     gulp.src('index.ru.src.html')
        .pipe(replace({
            patterns: [
                {
                    match: /version_number/g,
                    replacement: new Date().getTime()
                }
            ]
        }))
        .pipe(replace({
            patterns: [
                {
                    match: /css\/main\.css/g,
                    replacement: 'css/min/main.css'
                }
            ]
        }))
        .pipe(replace({
            patterns: [
                {
                    match: /js\/main\.js/g,
                    replacement: 'js/min/main.js'
                }
            ]
        }))
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(rename('index.html'))
        .pipe(gulp.dest('ru'));
});


gulp.task('default', function () {
    gulp.start('minify-main-css');
    gulp.start('minify-dd-css');
    gulp.start('minify-main-js');
    gulp.start('minify-html');
});
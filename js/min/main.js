function getTimeRemaining(endtime) {
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor((t / 1000) % 60);
    var minutes = Math.floor((t / 1000 / 60) % 60);
    var hours = Math.floor((t / (1000 * 60 * 60)) % 24);
    var days = Math.floor(t / (1000 * 60 * 60 * 24));
    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}

function initializeClock(id, endtime) {
    var clock = document.getElementById(id);
    var daysSpan = clock.querySelector('.days');
    var hoursSpan = clock.querySelector('.hours');
    var minutesSpan = clock.querySelector('.minutes');
    var secondsSpan = clock.querySelector('.seconds');

    function updateClock() {
        var t = getTimeRemaining(endtime);

        daysSpan.innerHTML = t.days;
        hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
        minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
        secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

        if (t.total <= 0) {
            clearInterval(timeinterval);
        }
    }

    updateClock();
    var timeinterval = setInterval(updateClock, 1000);
}

$(document).ready(function () {

    $('#countries').change(function() {
        var cntr=$(this).val();
        var hostname = window.location.origin;
        var urll=hostname+'/'+cntr;
        console.log(urll);
        window.location = urll;
    });


    if ($(window).width() < 768) {
        $('.emailinpt').attr('placeholder', 'E-mail');
    }

    $("#countries").msDropdown();


    var trigger = 'click';
    if ($(window).width() > 767) {
        trigger = 'hover';
    }

    $('.minicube .cimg').popover({
        html: true,
        placement: 'bottom',
        content: function () {
            return $(this).parent().find('.cdescr').html();
        },
        trigger: trigger
    });


    var deadline = new Date(Date.parse(new Date()) + 15 * 24 * 60 * 60 * 1000);
    initializeClock('clockdiv', deadline);


    var owl = $('.owl-carousel').owlCarousel({
        items: 3,

        merge: true,
        loop: true,
        margin: 20,
        padding: 20,
        video: true,
        lazyLoad: true,
        center: false,

        responsive: {
            1200: {
                items: 3,
                margin: 10,
                videoWidth: 300,
                videoHeight: 200,
            },
            800: {
                items: 3,
                margin: 10,

            },
            600: {
                items: 2,
                margin: 5
            },
            480: {
                items: 1,
                margin: 5
            }


        },

    });

    $('.owl-next').click(function () {
        owl.trigger('next.owl.carousel');
    });

    $('.owl-prev').click(function () {
        owl.trigger('prev.owl.carousel');
    });

    $('.owl-video-play-icon').on('click', function () {
        href = $(this).parent().find('.owl-video').attr('href');

        $('#bivideo').attr('src', href);

        return false;
    });


    var current_progress = 0;
    var interval = setInterval(function () {
        current_progress += 1;
        $(".scale .progress-bar")
            .css("width", current_progress + "%")
            .attr("aria-valuenow", current_progress);
        if (current_progress >= 31)
            clearInterval(interval);
    }, 240);


    $(window).scroll(function () {
        if ($(this).scrollTop() > 50) {
            $('#back-to-top').fadeIn();
        } else {
            $('#back-to-top').fadeOut();
        }
    });
    // scroll body to 0px on click
    $('#back-to-top').click(function () {
        $('#back-to-top').tooltip('hide');
        $('body,html').animate({
            scrollTop: 0
        }, 800);
        return false;
    });

    if($('#back-to-top').is(':visible')) {
        $('#back-to-top').tooltip('show');
    }


    function scrollNav() {
        $('nav a').click(function () {
            //Toggle Class
            $(".active").removeClass("active");
            $(this).closest('li').addClass("active");
            var theClass = $(this).attr("class");
            $('.' + theClass).parent('li').addClass('active');
            //Animate
            $('html, body').stop().animate({
                scrollTop: $($(this).attr('href')).offset().top
            }, 400);
            return false;
        });
        $('.scrollTop a').scrollTop();
    }

    scrollNav();

});